'use strict';

/**
 * Load xml meta when start the server
 */

var config = require('./config');

module.exports.init = function() {
var fs = require('fs'),
    xml2js = require('xml2js'),
    path = require("path"),
    mongoose = require('mongoose'),
    Meta = mongoose.model('Meta'),
    _ = require('lodash');


var parser = new xml2js.Parser({attrkey: 'attr'}),
    list = new Array();
	list = fs.readdirSync(config.metaPath);


	list.forEach(function(list) {
		console.log(list);
		if (path.extname(list) === ".xml") {
			var fd = fs.readFileSync(  config.metaPath + '/' + list, 'utf8');
			parser.parseString(fd, function (err, result) {
				var meta = new Meta({
			    content:  JSON.stringify(result),
			    title: list
				});
				meta.save();
			});
		}
	})
}