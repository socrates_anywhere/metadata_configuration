'use strict';

angular.module('mean.metas')
.controller('MetasController', ['$scope', '$stateParams', '$location', 'Global', 'Metas', function ($scope, $stateParams, $location, Global, Metas ) {
    $scope.global = Global;
    $scope.predicate = 'BusinessObjectType.attr.Name';
    $scope.reverse = true;

    $scope.create = function() {
        var meta = new Metas({
            title: this.title,
            content: this.content
        });
        meta.$save(function(response) {
            $location.path('metas/' + response._id);
        });

        this.title = '';
        this.content = '';
    };

    $scope.remove = function(meta) {
        if (meta) {
            meta.$remove();

            for (var i in $scope.metas) {
                if ($scope.metas[i] === meta) {
                    $scope.metas.splice(i, 1);
                }
            }
        }
        else {
            $scope.meta.$remove();
            $location.path('metas');
        }
    };

    $scope.update = function() {
        var meta = $scope.meta;
        if (!meta.updated) {
            meta.updated = [];
        }
        meta.updated.push(new Date().getTime());

        meta.$update(function() {
            $location.path('metas/' + meta._id);
        });
    };

    $scope.modify = function() {
        var meta = $scope.meta;
        
        meta.content = JSON.stringify($scope.root);         
        if (!meta.updated) {
            meta.updated = [];
        }
        meta.updated.push(new Date().getTime());

        meta.$update(function() {
            $location.path('metas/' + meta._id);
        });
    };

    $scope.modifyall = function() {
        var metas = $scope.metas;
        for (var i in metas) {
            metas[i].content = JSON.stringify($scope.roots[i]);         
            if (!metas[i].updated) {
                metas[i].updated = [];
            }
            metas[i].$update();
            $location.path('metas');
        }

    };

    $scope.find = function() {
        Metas.query(function(metas) {
            $scope.metas = metas;
            $scope.roots = new Array();
            for (var i in metas ) {         
                $scope.roots[i] = JSON.parse(metas[i].content);
            }
            
        });
    };



    $scope.findOne = function() {
        Metas.get({
            metaId: $stateParams.metaId
        }, function(meta) {
            $scope.meta = meta;
            $scope.root = JSON.parse(meta.content);          
        });
    };

}]);


