'use strict';

angular.module('mean.system').controller('HeaderController', ['$scope', 'Global', function ($scope, Global) {
    $scope.global = Global;

    $scope.menu = [{
        'title': 'Metadata List',
        'link': 'metas'
    }, {
        'title': 'Create New MetaData',
        'link': 'metas/create'
    },{
        'title': 'Upload New MetaData',
        'link': 'metas/upload'
    },{
        'title': 'MetaData table',
        'link': 'metas/table'
    },{
        'title': 'User List',
        'link': 'users'
    }];
    
    $scope.isCollapsed = false;
}]);