'use strict';

//Metas service used for metas REST endpoint
angular.module('mean.metas')
    .factory('Metas', ['$resource', function($resource) {
    return $resource('metas/:metaId', {
        metaId: '@_id'
    }, {
        update: {
            method: 'PUT'
        }
    });
}]);