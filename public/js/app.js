'use strict';

angular.module('mean', ['ngCookies', 'ngResource', 'ui.bootstrap', 'ui.router', 'mean.system', 'mean.metas', 'mean.users']);

angular.module('mean.system', []);
angular.module('mean.users', []);
angular.module('mean.metas', []);