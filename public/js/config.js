'use strict';

//Setting up route
angular.module('mean').config(['$stateProvider', '$urlRouterProvider',
  function($stateProvider, $urlRouterProvider) {
    // For unmatched routes:
    $urlRouterProvider.otherwise('/');

    // states for my app
    $stateProvider
      .state('all metas', {
        url: '/metas',
        templateUrl: 'views/metas/list.html'
    }).state('metas in table', {
        url: '/metas/table',
        templateUrl: 'views/metas/table.html'
    })
      .state('create meta', {
        url: '/metas/create',
        templateUrl: 'views/metas/create.html'
    })
      .state('edit meta', {
        url: '/metas/:metaId/edit',
        templateUrl: 'views/metas/edit.html'
    })
      .state('modify meta', {
        url: '/metas/:metaId/modify',
        templateUrl: 'views/metas/modify.html'
    })
      .state('upload meta', {
        url: '/metas/upload',
        templateUrl: 'views/metas/upload.html'
    })
      .state('meta by id', {
        url: '/metas/:metaId',
        templateUrl: 'views/metas/view.html'
    })
      .state('home', {
        url: '/',
        templateUrl: 'views/index.html'
    });
}
]);

//Setting HTML5 Location Mode
angular.module('mean').config(['$locationProvider',
  function($locationProvider) {
    $locationProvider.hashPrefix('!');
}
]);
