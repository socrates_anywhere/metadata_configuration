'use strict';

// Metas routes use metas controller
var metas = require('../controllers/metas');
var authorization = require('./middlewares/authorization');

// Meta authorization helpers
var hasAuthorization = function(req, res, next) {
	if (req.meta.user.id !== req.user.id) {
        return res.send(401, 'User is not authorized');
    }
    next();
};

module.exports = function(app) {

    app.get('/metas', metas.all);
    app.post('/metas', authorization.requiresLogin, metas.create);
    app.get('/metas/:metaId', metas.show);
    app.put('/metas/:metaId', authorization.requiresLogin, hasAuthorization, metas.update);
    app.del('/metas/:metaId', authorization.requiresLogin, hasAuthorization, metas.destroy);


    // Finish with setting up the metaId param
    app.param('metaId', metas.meta);

};