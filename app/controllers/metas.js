'use strict';


/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Meta = mongoose.model('Meta'),
    User = mongoose.model('User'),
    _ = require('lodash');


/**
 * Find meta by id
 */
exports.meta = function(req, res, next, id) {
    Meta.load(id, function(err, meta) {
        if (err) return next(err);
        if (!meta) return next(new Error('Failed to load meta ' + id));
        req.meta = meta;
        next();
    });
};

/**
 * Create an meta
 */
exports.create = function(req, res) {
    var meta = new Meta(req.body);
    meta.user = req.user;

    meta.save(function(err) {
        if (err) {
            return res.send('users/signup', {
                errors: err.errors,
                meta: meta
            });
        } else {
            res.jsonp(meta);
        }
    });
};

/**
 * Update an meta
 */
exports.update = function(req, res) {
    var meta = req.meta;

    meta = _.extend(meta, req.body);

    meta.save(function(err) {
        if (err) {
            return res.send('users/signup', {
                errors: err.errors,
                meta: meta
            });
        } else {
            res.jsonp(meta);
        }
    });
};

/**
 * Delete an meta
 */
exports.destroy = function(req, res) {
    var meta = req.meta;

    meta.remove(function(err) {
        if (err) {
            return res.send('users/signup', {
                errors: err.errors,
                meta: meta
            });
        } else {
            res.jsonp(meta);
        }
    });
};

/**
 * Show an meta
 */
exports.show = function(req, res) {
    res.jsonp(req.meta);
};

/**
 * List of Metas of current login user
 */
exports.all = function(req, res) {
    Meta.find( {user: req.user} ).sort('-created').populate('user', 'name username').exec(function(err, metas) {
        if (err) {
            res.render('error', {
                status: 500
            });
        } else {
            res.jsonp(metas);
        }
    });
};

